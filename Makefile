all:
	echo "Try: make save"

# save machine settings into dotfiles
save: save-files

save-files:
	cp --parents "${HOME}/.bash_aliases" .
	cp --parents "${HOME}/.bash_profile" .
	cp --parents "${HOME}/.bash_logout" .
	cp --parents "${HOME}/.bash_completion" .
	cp --parents "${HOME}/.bashrc" .
	cp --parents "${HOME}/.gitconfig" .
	cp --parents "${HOME}/.inputrc" .
	cp --parents "${HOME}/.vimrc" .
	cp --parents "${HOME}/.Xdefaults" .
	cp --parents "${HOME}/.xinitrc" .
	cp --parents "${HOME}/.xprofile" .
	cp --parents "${HOME}/.Xresources" .
	cp --parents "${HOME}/.xserverrc" .
	cp --parents "${HOME}/.profile" .
	cp --parents "${HOME}/.gnupg/gpg-agent.conf" .
	cp --parents -r "${HOME}/.config/sway/" .
	cp --parents -r "${HOME}/.config/i3/" .
	@echo "Saved all .files"
