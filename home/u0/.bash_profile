#!/bin/bash

command_exists () {
    type "$1" &> /dev/null ;
}

# define which terminal to use
if command_exists xterm ; then
    export TERMINAL=/usr/bin/xterm
fi
