alias c="cd ~/code"
alias la="ls -la --color"

# debug init, so if there are any error, we go in the logs
alias e="emacs -nw --debug-init"

alias newbg="feh --bg-center --bg-max -z ~/collections/*"

###
# pass
###

alias i4kpass="env PASSWORD_STORE_DIR=$HOME/.password-store/i4k pass"
alias u0pass="env PASSWORD_STORE_DIR=$HOME/.password-store/u0 pass"