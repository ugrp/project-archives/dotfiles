# import aliases
source "$HOME/.bash_aliases"

###
# compose key
###
# setxkbmap -option compose:ralt
swaymsg 'input * xkb_options compose:ralt'

###
# Bash prompt
###
# Set the terminal title to the current working directory
PS1="\[\033]0;\w\007\]"

# Build the prompt
PS1+="\u" # Username
PS1+="@" # @
PS1+="\h" # Host
PS1+=": " # :
PS1+="\w" # Working directory
PS1+="\n" # Newline
PS1+="\$ " # $ and a space

###
# Environment Variable
###
export EDITOR="emacs -nw"
export VISUAL=$EDITOR

export BROWSER=firefox-developer-edition
export PASSWORD_STORE_DIR=~/.password-store

###
# Path
###
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
PATH="$HOME/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:$HOME/go/bin:$HOME/.config/yarn/global/node_modules/.bin:$HOME/.cargo/bin:$GEM_HOME/bin"
export PATH

###
# User NVM (node version manager)
###
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

###
# gpg/ssh
###
# Configure pinentry to use the correct TTY
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null

# use gpg-agent to store SSH key instead of ssh-agent
# continue using ssh-add
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi

###
# Other
###

# golang path
export GOPATH="$HOME/go:$HOME/code/"

# rust/cargo path
source "$HOME/.cargo/env"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
